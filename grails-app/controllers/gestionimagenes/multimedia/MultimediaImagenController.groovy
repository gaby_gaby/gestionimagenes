package gestionimagenes.multimedia

import gestionimagenes.FileUploadService
import gestionimagenes.PropiedadImagen

class MultimediaImagenController {

	FileUploadService fileUploadService

	def viewImage(){
		println "MultimediaImagenController viewImage: " + params.id
		if (params.id != "null") {
			MultimediaImagen multimediaImagenInstance = MultimediaImagen.get((params.id) as Long)
			//println propiedadImagenInstance.imagen
			println fileUploadService.obtenerRutaAbsolutaMultimedia() + multimediaImagenInstance.obtenerRuta() + multimediaImagenInstance.imagen
			def file = new File(fileUploadService.obtenerRutaAbsolutaMultimedia() + multimediaImagenInstance.obtenerRuta() + multimediaImagenInstance.imagen)
			def img = file.bytes
			response.contentType = 'image/png' // or the appropriate image content type
			response.outputStream << img
			response.outputStream.flush()
		}
	}


}
