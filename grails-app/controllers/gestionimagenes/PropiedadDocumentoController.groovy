package gestionimagenes

import grails.transaction.Transactional;

@Transactional(readOnly = true)
class PropiedadDocumentoController {

	FileUploadService fileUploadService

	static allowedMethods = [create: ["GET", "POST"], delete: ["DELETE", "GET", "POST"]]

	@Transactional
	def create() {
		println "PropiedadDocumentoController create"
		println "params.id: " + params.id
		if (params.id) {
			def documento = request.documentoForm // request.getFile('documentoForm')
			if (documento) {
				println "${documento.getOriginalFilename()}"

				def validarDocumento = ""
				if (!documento.isEmpty()) {
					println "VALIDANDO DOCUMENTO"
					validarDocumento = fileUploadService.validarDocumento(documento, PropiedadDocumento.sizeMax)
					if (validarDocumento == "") { //solo se inserta si se valida la imagen
						PropiedadDocumento propiedadDocInstance = new PropiedadDocumento(documento, (params.id) as Long)
						if(propiedadDocInstance.save(flush:true)) {
							// Save imagenPrincipal if uploaded
							println "CREANDO DOCUMENTO"
							propiedadDocInstance.documento = fileUploadService.uploadFile(documento, "${propiedadDocInstance.propiedad.id}-${propiedadDocInstance.id}-${documento.getOriginalFilename()}", propiedadDocInstance.obtenerRuta())
							propiedadDocInstance.save(flush:true)
							println "DOCUMENTO GUARDADO"
							flash.messageDocumento = message(code: 'default.created.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), propiedadDocInstance.id])
						}
					}
					flash.messageDocumento = flash.messageDocumento + ". " + validarDocumento
				}
				else {
					flash.messageDocumento = "Debe seleccionar un documento"
				}

			}
		}
		Propiedad pi = Propiedad.get(params.id)
		redirect controller:pi.getClass().simpleName, action:"edit", id:params.id, fragment:'panelDocumentos'
	}

	@Transactional
	def delete() {
		println "delete PropiedadDocumentoController nuevo: id:" + params.id
		def propiedadDocumentoInstance = PropiedadDocumento.get(params.id)
		if (propiedadDocumentoInstance) {
			try {
				def controlladorDestino = propiedadDocumentoInstance.propiedad.getClass().simpleName
				def idDestino = propiedadDocumentoInstance.propiedad.id

				propiedadDocumentoInstance.delete(flush:true)
				println "documento borrado"

				flash.messageImage = "${message(code: 'default.deleted.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), propiedadDocumentoInstance])}"
				//redirect(action: "list")
				redirect controller:controlladorDestino, action:'edit', id:idDestino, fragment:'panelDocumentos'
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				log.error("Delete Exception: " + e)
				flash.messageImage = "${message(code: 'default.not.deleted.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.messageImage = "${message(code: 'default.not.found.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), params.id])}"
			redirect(action: "list")
		}
	}
}
