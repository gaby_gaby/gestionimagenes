package gestionimagenes

import grails.transaction.Transactional;

@Transactional(readOnly = true)
class PropiedadVideoController {

    static allowedMethods = [create: ["GET", "POST"], delete: ["DELETE", "GET", "POST"]]

	@Transactional
	def create() {
		println "PropiedadVideoController create"
		println "params.id: " + params.id
		
		if (params.id) {
			def video =  params.urlVideoForm
			def nombre = params.nombreVideoForm
			if (video) {
				println "VALIDANDO DOCUMENTO"
				if (nombre) {
					PropiedadVideo propiedadVideoInstance = new PropiedadVideo(nombre, (params.id) as Long)
					propiedadVideoInstance.urlVideo = video
					
					if (propiedadVideoInstance.direccionVideoValida()) {
						if(propiedadVideoInstance.save(flush:true)) {
							flash.messageVideo = message(code: 'default.created.message', args: [message(code: 'multimediaVideo.label', default: 'Video'), propiedadVideoInstance.id])
						}
					}
					else {
						flash.messageVideo = "Dirección de vídeo incorrecta"
					}
				}
				else {
					flash.messageVideo = "Debe seleccionar introducir el nombre del vídeo"
				}
			}
			else {
				flash.messageVideo = "Debe seleccionar introducir la url de un video"
			}
		}				
		Propiedad pi = Propiedad.get(params.id)
		redirect controller:pi.getClass().simpleName, action:"edit", id:params.id, fragment:'panelVideos'
	 }
	
	@Transactional
	def delete() {
		println "delete PropiedadVideoController nuevo: id:" + params.id
		def propidadVideoInstance = PropiedadVideo.get(params.id)
		if (propidadVideoInstance) {
			try {
				def controlladorDestino = propidadVideoInstance.propiedad.getClass().simpleName
				def idDestino = propidadVideoInstance.propiedad.id
				
				propidadVideoInstance.delete(flush:true)
				println "video borrado"
				
				flash.messageVideo = "${message(code: 'default.deleted.message', args: [message(code: 'multimediaVideo.label', default: 'Video'), propidadVideoInstance])}"
				//redirect(action: "list")
				redirect controller:controlladorDestino, action:'edit', id:idDestino, fragment:'panelVideos'
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				log.error("Delete Exception: " + e)
				flash.messageVideo = "${message(code: 'default.not.deleted.message', args: [message(code: 'multimediaVideo.label', default: 'Video'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.messageVideo = "${message(code: 'default.not.found.message', args: [message(code: 'multimediaVideo.label', default: 'Video'), params.id])}"
			redirect(action: "list")
		}
	}
}
