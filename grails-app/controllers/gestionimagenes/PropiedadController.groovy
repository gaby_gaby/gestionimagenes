package gestionimagenes

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PropiedadController {
	//def scaffold = true		
	
    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]
	
	FileUploadService fileUploadService		

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Propiedad.list(params), model:[propiedadInstanceCount: Propiedad.count()]
    }

    def show(Propiedad propiedadInstance) {
        respond propiedadInstance
    }

    def create() {
        respond new Propiedad(params)
    }

    @Transactional
    def save(Propiedad propiedadInstance) {
        if (propiedadInstance == null) {
            notFound()
            return
        }

        if (propiedadInstance.hasErrors()) {
            respond propiedadInstance.errors, view:'create'
            return
		}

		if(propiedadInstance.save(flush:true)) {		
			request.withFormat {
				form multipartForm {
					flash.message = message(code: 'default.created.message', args: [message(code: 'propiedad.label', default: 'Propiedad'), propiedadInstance.id])										
					redirect propiedadInstance
				}
				'*' { respond propiedadInstance, [status: CREATED] }
			}
		}       
    }

    def edit(Propiedad propiedadInstance) {
        respond propiedadInstance
    }

    @Transactional
    def update(Propiedad propiedadInstance) {
        if (propiedadInstance == null) {
            notFound()
            return
        }

        if (propiedadInstance.hasErrors()) {
            respond propiedadInstance.errors, view:'edit'
            return
        }
			
       if(propiedadInstance.save(flush:true)) {						
			 request.withFormat {
				form multipartForm {
					flash.message = message(code: 'default.updated.message', args: [message(code: 'Propiedad.label', default: 'Propiedad'), propiedadInstance.id])
					redirect propiedadInstance
				}
				'*'{ respond propiedadInstance, [status: OK] }
			}
		}       
    }

    @Transactional
    def delete(Propiedad propiedadInstance) {
		println ("delete")
        if (propiedadInstance == null) {
            notFound()
            return
        }
		
		propiedadInstance.delete(flush:true)
				
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Propiedad.label', default: 'Propiedad'), propiedadInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'propiedad.label', default: 'Propiedad'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

	@Transactional
	def addImagenPrincipal() {
		forward controller: 'propiedadImagenPrincipal', action:'create', model:[imagenPrincipalForm:request.getFile('imagenPrincipalForm')], params:params
	}
		
	@Transactional
	def addImagen() {
		forward controller: 'propiedadImagen', action:'create', model:[imagenForm:request.getFile('imagenForm')], params:params
	}
	
	@Transactional
	def addPlano() {				
		forward controller: 'propiedadPlano', action:'create', model:[planoForm:request.getFile('planoForm')], params:params				
	}
	
	@Transactional
	def addDocumento() {				
		forward controller: 'propiedadDocumento', action:'create', model:[documentoForm:request.getFile('documentoForm')], params:params
	}
		
	@Transactional
	def addVideo() {				
		forward controller: 'propiedadVideo', action:'create', params:params				
	}
}
