package gestionimagenes

import grails.transaction.Transactional;

@Transactional(readOnly = true)
class EntidadDocumentoController {

	FileUploadService fileUploadService

	static allowedMethods = [create: ["GET", "POST"], delete: ["DELETE", "GET", "POST"]]

	@Transactional
	def create() {
		println "EntidadDocumentoController create"
		println "params.id: " + params.id
		if (params.id) {
			def documento = request.documentoForm // request.getFile('documentoForm')
			if (documento) {
				println "${documento.getOriginalFilename()}"

				def validarDocumento = ""
				if (!documento.isEmpty()) {
					println "VALIDANDO DOCUMENTO"
					validarDocumento = fileUploadService.validarDocumento(documento, EntidadDocumento.sizeMax)
					if (validarDocumento == "") { //solo se inserta si se valida la imagen
						EntidadDocumento entidadDocInstance = new EntidadDocumento(documento, (params.id) as Long)
						if(entidadDocInstance.save(flush:true)) {
							// Save imagenPrincipal if uploaded
							println "CREANDO DOCUMENTO"
							entidadDocInstance.documento = fileUploadService.uploadFile(documento, "${entidadDocInstance.entidad.id}-${entidadDocInstance.id}-${documento.getOriginalFilename()}", entidadDocInstance.obtenerRuta())
							entidadDocInstance.save(flush:true)
							println "DOCUMENTO GUARDADO"
							flash.messageDocumento = message(code: 'default.created.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), entidadDocInstance.id])
						}
					}
					flash.messageDocumento = flash.messageDocumento + ". " + validarDocumento
				}
				else {
					flash.messageDocumento = "Debe seleccionar un documento"
				}

			}
		}
		Entidad pi = Entidad.get(params.id)
		redirect controller:pi.getClass().simpleName, action:"edit", id:params.id, fragment:'panelDocumentos'
	}

	@Transactional
	def delete() {
		println "delete EntidadDocumentoController nuevo: id:" + params.id
		def EntidadDocumentoInstance = EntidadDocumento.get(params.id)
		if (EntidadDocumentoInstance) {
			try {
				def controlladorDestino = EntidadDocumentoInstance.entidad.getClass().simpleName
				def idDestino = EntidadDocumentoInstance.entidad.id

				EntidadDocumentoInstance.delete(flush:true)
				println "documento borrado"

				flash.messageImage = "${message(code: 'default.deleted.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), EntidadDocumentoInstance])}"
				//redirect(action: "list")
				redirect controller:controlladorDestino, action:'edit', id:idDestino, fragment:'panelDocumentos'
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				log.error("Delete Exception: " + e)
				flash.messageImage = "${message(code: 'default.not.deleted.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.messageImage = "${message(code: 'default.not.found.message', args: [message(code: 'multimediaDocumento.label', default: 'Documento'), params.id])}"
			redirect(action: "list")
		}
	}
}
