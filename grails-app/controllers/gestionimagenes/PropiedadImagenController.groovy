package gestionimagenes
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


@Transactional(readOnly = true)
class PropiedadImagenController {
	//def scaffold = true
	
	FileUploadService fileUploadService
	
    static allowedMethods = [create: ["GET", "POST"], delete: ["DELETE", "GET", "POST"]]
		
	@Transactional
	def create() {
		println "PropiedadImagenController create"
		println "params.id: " + params.id
		
		if (params.id) {
			def imagen = request.imagenForm //request.getFile('imagenForm')
			if (imagen) {
				def validarImagen = ""
				if (!imagen.isEmpty()) {
					println "VALIDANDO IMAGEN"
					//validarImagen = fileUploadService.validarImagen(imagen, PropiedadImagen.sizeMax)
					validarImagen = fileUploadService.validarImagen(imagen, 0)
					if (validarImagen == "") { //solo se inserta si se valida la imagen
						PropiedadImagen propiedadPlanoInstance = new PropiedadImagen(imagen, (params.id) as Long)
						if(propiedadPlanoInstance.save(flush:true)) {
							// Save imagenPrincipal if uploaded
							println "CREANDO IMAGEN"
							//propiedadPlanoInstance.imagen = fileUploadService.uploadFile(imagen, "${propiedadPlanoInstance.propiedad.id}-${propiedadPlanoInstance.id}.png", propiedadPlanoInstance.obtenerRuta())
							propiedadPlanoInstance.imagen = fileUploadService.uploadImagenEscalada(imagen, "${propiedadPlanoInstance.propiedad.id}-${propiedadPlanoInstance.id}.png", propiedadPlanoInstance.obtenerRuta())
							propiedadPlanoInstance.save(flush:true)
							println "IMAGEN GUARDADA"
							flash.messageImage = message(code: 'default.created.message', args: [message(code: 'multimediaImagen.label', default: 'Imagen'), propiedadPlanoInstance.id])
						}
					}
					flash.messageImage = flash.messageImage + ". " + validarImagen
				}
				else {
					flash.messageImage = "Debe seleccionar imagen"
				}
								
			}
		}
		Propiedad pi = Propiedad.get(params.id)
		redirect controller:pi.getClass().simpleName, action:"edit", id:params.id, fragment:'panelImagenes'
	}

	@Transactional
	def delete() {
		println "delete propiedadImagenController nuevo: id:" + params.id
		def propiedadImagenInstance = PropiedadImagen.get(params.id)
		if (propiedadImagenInstance) {
			try {
				def controlladorDestino = propiedadImagenInstance.propiedad.getClass().simpleName
				def idDestino = propiedadImagenInstance.propiedad.id
				
				propiedadImagenInstance.delete(flush:true)
				println "imagen borrada"
				
				flash.messageImage = "${message(code: 'default.deleted.message', args: [message(code: 'multimediaImagen.label', default: 'Imagen'), propiedadImagenInstance])}"
				//redirect(action: "list")
				redirect controller:controlladorDestino, action:'edit', id:idDestino, fragment:'panelImagenes'
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				log.error("Delete Exception: " + e)
				flash.messageImage = "${message(code: 'default.not.deleted.message', args: [message(code: 'multimediaImagen.label', default: 'Imagen'), params.id])}"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.messageImage = "${message(code: 'default.not.found.message', args: [message(code: 'multimediaImagen.label', default: 'Imagen'), params.id])}"
			redirect(action: "list")
		}
	}
}
