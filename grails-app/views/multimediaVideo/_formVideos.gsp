<g:if test="${contenedor.id}">	
		<div class="panel panel-default" id="panelVideos">
			<div class="panel-heading">
				<g:message code="multimediaVideos.label" default="Videos" />
				<span class="badge">${contenedor?.videos?.size()}</span>
				<a data-toggle="collapse" data-parent="#accordion" href="#addVideo" class="btn btn-default btn-xs pull-right"
					title="${message(code:'default.add.label', args: [message(code: 'multimediaVideo.label', default: 'Video')])}">
					<i class="glyphicon glyphicon-plus"></i>
				</a>
			</div>
			<div class="panel-body">
				<g:if test="${flash.messageVideo}">					
					<bootstrap:alert class="alert alert-info">${flash.messageVideo}</bootstrap:alert>
				</g:if>
				
				<div id="addVideo" class="panel-collapse collapse">
					<g:if test="${contenedor.puedeAñadirNuevoVideo()}">
						<div class="row">
							<div class="col-md-12">
								<div class="well well-sm">
									<label>
										${message(code: 'default.add.label', args: [message(code: 'multimediaVideo.label', default: 'Video')])}
									</label>								
									<div class="form-group">
										<label for="nombreVideoForm" class="col-sm-2 control-label text-danger">										
											<g:message code="multimediaVideo.nombre.label" default="Nombre" />
											<span class="required-indicator danger">*</span>
										</label>
										<div class="col-sm-9">	
											<g:textField name="nombreVideoForm" class="form-control" value=""/>						
										</div>		
									</div>
																 
									<div class="form-group">
										<label for="urlVideoForm" class="col-sm-2 control-label text-danger">										
											<g:message code="multimediaVideo.urlVideo.label" default="URL" />
											<span class="required-indicator danger">*</span>
										</label>
										<div class="col-sm-9">	
											<g:textField name="urlVideoForm" class="form-control" value=""/>
											<span class="help-block">
												<g:message code="multimediaVideos.ayudaSubida.label" default="URL vídeo de Youtube (https://youtu.be/4QZvLSGuYoo) o de Vimeo (https://vimeo.com/channels/staffpicks/138645198)" />
											</span>
										</div>		
									</div>																
									
									<g:actionSubmit value="${message(code:'default.add.label', args: [' '])}" 
												action="addVideo" class="btn btn-primary pull-right" 
												title="${message(code:'default.add.label', args: [message(code: 'multimediaVideo.label', default: 'Video')])}" />
									<br>
									<br>
									
								</div>
							</div>						
						</div>					
					</g:if>
					<g:else>
						<div class="well well-sm">
							<g:message code="multimediaDocumentos.multimediaVideos.label" default="Máximo de videos alcanzado" />
						</div>
					</g:else>	
				</div>

				<g:if test="${contenedor?.videos?.size()>0}">
					<div class="row">
						<% int j = 1; %>
						<g:each in="${contenedor?.videos?.sort{it.id}}" var="i">							
							<g:render template="/multimediaVideo/formVideo" model="[i:i, j:j]" />
							<% j++; %>														 
						</g:each>
					</div>
				</g:if>													
			</div>
		</div>
</g:if>