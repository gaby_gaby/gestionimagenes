<div class="col-xs-12 col-sm-6 col-md-3">
	<div class="thumbnail">
		<g:if test="${i.direccionVideoValida()}">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="${ i.obtenerDireccionEmbeber() }" 
					frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</g:if>
		<g:else>
			El vídeo no está alojado ni en Youtube ni en Vimeo.
		</g:else>
		
		<div class="caption">
			<p>
				${i.nombre }
			</p>						
		</div>
	</div>
</div>