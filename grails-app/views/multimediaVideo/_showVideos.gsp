<g:if test="${contenedor?.videos}">
	<div class="panel panel-default">
		<div class="panel-heading">
			<g:message code="contenedor.videos.label" default="Videos" />
			<span class="badge">${contenedor?.videos?.size()}</span>			
		</div>
		<div class="panel-body text-center">			
			<div class="coverimg">
				<g:each in="${contenedor?.videos?.sort{it.id}}" var="i">
					<g:render template="/multimediaVideo/showVideo" model="[i:i]" />						
				</g:each>												
			</div>
		</div>
	</div>
</g:if>

