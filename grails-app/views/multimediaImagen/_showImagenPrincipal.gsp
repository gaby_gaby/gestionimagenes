<g:if test="${contenedor?.imagenPrincipal}">
	<div class="panel panel-default">
		<div class="panel-heading">
			<g:message code="propiedad.imagenPrincipal.label" default="Imagen Principal" />						
		</div>
		<div class="panel-body text-center">					
			
			<span class="property-value" aria-labelledby="imagenPrincipal-label">
					<img src="<g:createLink controller="multimediaImagen" action="viewImage" params="[id:"${contenedor.imagenPrincipal.id}" ]"/>"
						width="150px" class="imagenSobra img-rounded"/>
			</span>
		</div>
	</div>
</g:if>