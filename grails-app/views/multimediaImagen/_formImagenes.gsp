<g:if test="${contenedor.id}">
	<div class="panel panel-default" id="panelImagenes">
		<div class="panel-heading">
			<g:message code="multimediaImagenes.label" default="Imagenes" />
			<span class="badge"> ${contenedor?.imagenes?.size()}</span>
			<a data-toggle="collapse" data-parent="#accordion" href="#addImagen" class="btn btn-default btn-xs pull-right"
				title="${message(code:'default.add.label', args: [message(code: 'multimediaImagen.label', default: 'Imagen')])}">
				<i class="glyphicon glyphicon-plus"></i>
			</a>
		</div>
		<div class="panel-body">
			<g:if test="${flash.messageImage}">				
				<bootstrap:alert class="alert alert-info">${flash.messageImage}</bootstrap:alert>
			</g:if>
			
			<div id="addImagen" class="panel-collapse collapse">
				<g:if test="${contenedor.puedeAñadirNuevaImagen()}">
					<div class="row">
						<div class="col-md-12">							
							<div class="well well-sm">
								<div class="form-group">
									<div class="col-sm-3 control-label">
										<label for="imagen"> ${message(code: 'default.add.label', args: [message(code: 'multimediaImagen.label', default: 'Imagen')])}
										</label>
									</div>
									<div class="col-sm-9">
										<input type="file" name="imagenForm" id="imagenForm" accept="image/png, image/jpg, image/jpeg"
											onchange="validarFormatoImagenes(this);$('#imagenFormVisible').val($(this).val().split('\\').pop());"
											class="hidden" />
	
										<div class="input-group">
											<span class="input-group-btn"> <a class="btn btn-default" onclick="$('#imagenForm').click();"
												title="Seleccionar nueva imagen">Sel.</a>
											</span> 
											<input type="text" name="imagenFormVisible" id="imagenFormVisible" class="form-control" disabled /> 
											
											<span class="input-group-btn"> <g:actionSubmit
													value="${message(code:'default.add.label', args: [' '])}" action="addImagen"
													class="btn btn-primary"
													title="${message(code:'default.add.label', args: [message(code: 'multimediaImagen.label', default: 'Imagen')])}" />
											</span>
										</div>
	
										<p class="help-block">									
											<g:message code="multimediaImagenes.ayudaSubida.label" default="Ficheros .jpg o .png de un máximo de 512KB" />
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</g:if>
				<g:else>
					<div class="well well-sm">					
						<g:message code="multimediaImagenes.maximoAlcanzado.label" default="Máximo de imágenes alcanzado" />
					</div>
				</g:else>
			</div>

			<g:if test="${contenedor?.imagenes?.size()>0}">
				<div class="row">
					<% int j = 1; %>
					<g:each in="${contenedor?.imagenes?.sort{it.id}}" var="i">
						<div class="col-xs-6 col-sm-4 col-md-3">
							<div class="thumbnail">
								<img src="<g:createLink controller='multimediaImagen' action='viewImage' params='[id:" ${i.id} "]' />"
									class="img-responsive img-rounded" />

								<div class="caption">
									<p>
										<span class="badge">
											${j} <% j++; %>
										</span>
										${i.nombre }
									</p>
									<p class="text-center">
										<small>${i.tamanoKB }KB</small><br>
										<small><g:formatDate format="yyyy-MM-dd HH:mm:ss a" date="${i.dateCreated}"/></small>
									</p>

									<g:link controller="${i.getClass().simpleName}" action="delete" id="${i.id}"
										class="btn btn-xs btn-danger pull-right"
										onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
										<span class="glyphicon glyphicon-remove"></span>Eliminar
     										</g:link>
									<br>
								</div>
							</div>
						</div>
					</g:each>
				</div>
			</g:if>			
		</div>
	</div>
</g:if>