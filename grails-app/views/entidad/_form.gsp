<%@ page import="gestionimagenes.Entidad" %>

<div class="form-group">
	<div class="col-sm-2 control-label text-danger">
		<label for="nombre">
			<g:message code="entidad.nombre.label" default="Nombre" />
			<span class="required-indicator">*</span>
		</label>
	</div>
	<div class="col-sm-10">
		<g:textField name="nombre" class="form-control" value="${entidadInstance?.nombre}"/>
	</div>
</div>

<g:render template="/multimediaImagen/formImagenPrincipal" bean="${entidadInstance}" var="contenedor" />

<g:render template="/multimediaDocumento/formDocumentos" bean="${entidadInstance}" var="contenedor" />

