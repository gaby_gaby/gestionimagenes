// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require_tree .
//= require_self
//= require bootstrap

//Validación extensión fichero .jpg o .png (a utilizar en las imágenes)
function validarFormatoImagenes(element) {
	if (element.value!=null && element.value!='') {
		//var ext = element.value.match(/\.(.+)$/)[1];
		var ext = element.value.split('.').pop();
		ext = ext.toLowerCase();
		//alert(ext);
	    switch(ext)
	    {
	        case 'jpg':
	        case 'jpeg':
	        case 'png':        
	            //alert('allowed');
	            break;
	        default:
	            alert('Formato imagen no válido (' + ext + ')');
	        	element.value='';
	    }
	}
	else {
		return true
	}
}

function validarTamanoMultimedia(element, maximoPermitido) {	
	//maximoPermitido: expresado en KB
	if	(typeof element.files[0] != 'undefined') {
		var size = element.files[0].size;
		size = Math.round(size / 1024); //para pasarlo a KB
		if (size > maximoPermitido) {
			alert('Fichero demasiado pesado. (' + size + 'KB) Maximo permitido: ' + maximoPermitido + 'KB')			
			element.value='';
		}
	}
}

function validarFormatoDocumentos(element) {
	if (element.value!=null && element.value!='') {
		var ext = element.value.split('.').pop();
		ext = ext.toLowerCase();		
	    switch(ext)
	    {
	        case 'xlsx':
	        case 'xls':
	        case 'doc':
	        case 'docx':
	        case 'pptx':
	        case 'ppt':
	        case 'txt':
	        case 'pdf':
	            //alert('allowed');
	            break;
	        default:
	            alert('Formato imagen no válido (' + ext + ')');
	        	element.value='';
	    }
	}
	else {
		return true
	}
}


function inicializarGaleriaImagenes() {
	$(document).jquerygallery({
		// displays a thumbnails navigation
		'coverImgOverlay' : true,

		// CSS classes
		'imgActive' : "imgActive",
		'thumbnail' : "coverImgOverlay",
		'overlay' : "overlay",

		// the height of the thumbnails
		'thumbnailHeight' : 90,

		// custom navigation controls. 
		// requires Font Awesome
		'imgNext' : "<span class='glyphicon glyphicon-menu-right'></span>",
		'imgPrev' : "<span class='glyphicon glyphicon-menu-left'></span>",
		'imgClose' : "<span class='glyphicon glyphicon-remove'></span>",

		// animation speed
		'speed' : 300
	});
}