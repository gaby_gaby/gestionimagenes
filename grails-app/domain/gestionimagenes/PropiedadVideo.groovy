package gestionimagenes

import gestionimagenes.multimedia.MultimediaVideo

import org.springframework.web.multipart.MultipartFile;

class PropiedadVideo extends MultimediaVideo {
	
	static belongsTo = [propiedad:Propiedad]

    static constraints = {
		propiedad nullable:false, blank:false
    }
	
	PropiedadVideo(String nombre, Long propiedadId) {
		this.nombre = nombre		
		this.propiedad = Propiedad.get(propiedadId)
	}
	
	static mapping = {
		propiedad fetch: 'join'
	}
	
	public String obtenerRuta() {
		return this.urlVideo
	}
	
	public String getTipo() {
		return "Video propiedad"
	}
	
	public getContenedor() {
		return this.propiedad
	}
}
