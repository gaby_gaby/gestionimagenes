package gestionimagenes.multimedia

abstract class MultimediaImagen extends Multimedia {
	
	static transients = ['ruta']

	private static String ruta = ""
	
	String imagen
	
    static constraints = {
		imagen nullable:true, blank:true
    }
	
	def afterDelete() {
		//Borrar el fichero del sistema de ficheros
		println "MultimediaImagen - afterDelete()"
		println this.imagen
				
		if (this.imagen != "") {
			if (!fileUploadService.removeFile(this.imagen, this.obtenerRuta())) {
				println "NO SE HA BORRADO EL FICHERO: " + this.obtenerRuta() + this.imagen
			}
		}
	}
	
	String toString() {
		return this.imagen
	}
	
	public abstract String obtenerRuta()
	
	public abstract String getTipo()
	
	public abstract getContenedor()
	
}
