package gestionimagenes

import gestionImagenes.multimedia.iMultimedia;

class Entidad implements iMultimedia {
	
	String nombre
	
	EntidadImagenPrincipal imagenPrincipal
	
	static hasMany = [documentos:EntidadDocumento]

    static constraints = {
		nombre nullable:false, blank:false
		imagenPrincipal nullable:true, blank:true
		documentos nullable:true, blank:true
    }

	@Override
	public boolean puedeAñadirNuevaImagen() {
		// Sin limites
		return true;
	}
	@Override
	public boolean puedeAñadirNuevoPlano() {
		// Entidades no tienen planos
		return false;
	}
	@Override
	public boolean puedeAñadirNuevoDocumento() {
		//Sin limites
		return true;
	}
	@Override
	public boolean puedeAñadirNuevoVideo() {
		//Sin limites
		return true;
	}}
