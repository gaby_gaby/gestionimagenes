package gestionimagenes

import org.springframework.web.multipart.MultipartFile;
import gestionimagenes.multimedia.MultimediaImagen;

class PropiedadImagenPrincipal extends MultimediaImagen {

	//static int sizeMax = 512 //KB
	
	private static String ruta = "propiedad/principales/"
	
	static belongsTo = [propiedad:Propiedad]
	
    static constraints = {
		propiedad nullable:false, blank:false				
    }
	
	static mapping = {
		propiedad fetch: 'join'
	}
	
	def afterDelete() {
		println "afterDelete()"	
		super.afterDelete()
	}
	
	PropiedadImagenPrincipal(MultipartFile file, Long propiedadId) {
		this.nombre = file.getOriginalFilename()
		this.tamanoKB = Math.round(file.getSize() / 1024)
		this.propiedad = Propiedad.get(propiedadId)
	}
	
	public String obtenerRuta() {
		return MultimediaImagen.ruta + PropiedadImagenPrincipal.ruta + this.propiedad.id.toString() + "/"
	}
	
	public String getTipo() {
		return "Imagen principal propiedad"
	}
	
	public getContenedor() {
		return this.propiedad
	}
	
}
